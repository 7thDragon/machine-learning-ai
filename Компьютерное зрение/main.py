import tensorflow as tf


class GAN:

    def __init__(self,
                 image_shape,
                 BATCH_SIZE,
                 BUFFER_SIZE,
                 alpha=0.2,
                 hidden_dim=100,
                 MAX_PRINT_LABELS=20
                 ):
        """
        Инициализация параметров
        """
        self.hidden_dim = hidden_dim
        self.alpha = alpha
        self.image_shape = image_shape
        self.cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        self.gen_optimizer = tf.keras.optimizers.Adam(1e-4)
        self.disc_optimizer = tf.keras.optimizers.Adam(1e-4)
        self.BATCH_SIZE = BATCH_SIZE
        self.MAX_PRINT_LABELS = MAX_PRINT_LABELS
        self.BUFFER_SIZE = BUFFER_SIZE

    def generator(self):
        """
        Создание модели генератора
        :return: model
        """
        generator = tf.keras.models.Sequential([
            layers.Dense(4 * 4 * 1024, input_shape=(self.hidden_dim,)),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),
            layers.Reshape((4, 4, 1024)),

            layers.Conv2DTranspose(256, (5, 5), strides=(2, 2), padding="same"),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),

            layers.Conv2DTranspose(128, (5, 5), strides=(2, 2), padding="same"),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),

            layers.Conv2DTranspose(64, (5, 5), strides=(2, 2), padding="same"),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),

            layers.Conv2DTranspose(3, (5, 5), strides=(2, 2), padding="same", activation="sigmoid"),
        ], name="generator")
        generator.summary()

        return generator

    def discriminator(self):
        """
        Создание модели дискриминатора
        :return: model
        """
        discriminator = tf.keras.models.Sequential([
            layers.Conv2D(64, (5, 5), strides=(2, 2), padding="same", input_shape=self.image_shape),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),

            layers.Conv2D(128, (5, 5), strides=(2, 2), padding="same"),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),

            layers.Conv2D(256, (5, 5), strides=(2, 2), padding="same"),
            layers.BatchNormalization(),
            layers.LeakyReLU(self.alpha),

            layers.Flatten(),
            layers.Dense(1)
        ], name="discriminator")
        discriminator.summary()

        return discriminator

    def generator_loss(self, fake_output):
        """
        Функция потерь генератора
        :param fake_output: число от 0 до 1
        :return: значение функции потерь
        """
        return self.cross_entropy(tf.ones_like(fake_output), fake_output)

    def discriminator_loss(self, real_output, fake_output):
        """
        Функция потерь дискриминатора
        :param real_output: число от 0 до 1 для настоящей каритнки
        :param fake_output: число от 0 до 1 для сгенерированной картинки
        :return: значение функции потерь
        """
        real_loss = self.cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = self.cross_entropy(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        return total_loss

    @tf.function
    def train_step(self, generator, discriminator, images):
        noise = tf.random.normal([self.BATCH_SIZE, self.hidden_dim])

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_images = generator(noise, training=True)

            real_output = discriminator(images, training=True)
            fake_output = discriminator(generated_images, training=True)

            gen_loss = self.generator_loss(fake_output)
            disc_loss = self.discriminator_loss(real_output, fake_output)

        gradients_gen = gen_tape.gradient(gen_loss, generator.trainable_variables)
        gradients_disc = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

        self.gen_optimizer.apply_gradients(zip(gradients_gen, generator.trainable_variables))
        self.disc_optimizer.apply_gradients(zip(gradients_disc, discriminator.trainable_variables))

        return gen_loss, disc_loss

    def train(self, dataset, epochs, generator, discriminator):

        history = []
        th = self.BUFFER_SIZE // (self.BATCH_SIZE * self.MAX_PRINT_LABELS)

        for epoch in range(1, epochs + 1):
            print(f"{epoch} эпоха из {epochs}: ", end="")

            start = time.time()
            n = 0

            gen_loss_epoch = 0
            for images_batch in dataset:
                gen_loss, disc_loss = self.train_step(generator, discriminator, images_batch)
                gen_loss_epoch += K.mean(gen_loss)
                if n % th == 0:
                    print('=', end='')
                n += 1

            if (epoch + 1) % 100 == 0:
                generator.save(f"models/generator{epoch + 1996}")
                discriminator.save(f"models/discriminator{epoch + 1996}")

            history.append(gen_loss_epoch / n)
            print(": " + str(history[-1]))
            print("Время эпохи {} состовляет {} секунд".format(epoch, time.time() - start))

        return history

    def generate_and_save_images(self, generator, quantity):
        noise = tf.random.normal([quantity, self.hidden_dim])
        predictions = generator(noise, training=False)

        for i in range(predictions.shape[0]):
            fig = plt.figure(figsize=(5, 5))
            plt.imshow(predictions[i])
            plt.axis('off')
            plt.savefig('generated_cats/image_at_epoch_{:04d}.png'.format(i))
            plt.show()


if __name__ == '__main__':
    import os
    import cv2 as cv

    import numpy as np

    os.add_dll_directory("C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v12.1/bin")
    import matplotlib.pyplot as plt
    from tensorflow.keras import layers
    import time


    import tensorflow.keras.backend as K

    # img_size = 64
    # batch_size = 128
    # # cat_dataset = tf.keras.preprocessing.image_dataset_from_directory(
    # #     "classes",
    # #     labels="inferred",
    # #     label_mode="int",
    # #     class_names=['cats'],
    # #     color_mode="rgb",
    # #     batch_size=batch_size,
    # #     image_size=(img_size, img_size),
    # #     shuffle=True,
    # #     seed=None,
    # #     validation_split=None,
    # #     subset=None,
    # #     interpolation="bilinear",
    # #     follow_links=False,
    # # )
    # #
    # # cat_train_labels = []
    # # cat_train_images = []
    # #
    # # for images, labels in cat_dataset:
    # #     for i in range(len(images)):
    # #         cat_train_images.append(images[i])
    # #         cat_train_labels.append(labels[i])
    # #
    # # c_images = np.array(cat_train_images)
    # # print(c_images.shape)
    # # c_labels = np.array(cat_train_labels)
    # # print(c_labels.shape)
    # # np.save("np/images.npy", c_images)
    # # np.save("np/labels.npy", c_labels)
    # with open("np/images.npy", "rb") as im:
    #     c_images = np.load(im)
    # with open("np/labels.npy", "rb") as lb:
    #     c_labels = np.load(lb)
    #
    # buffer_size = c_images.shape[0]
    # buffer_size = buffer_size // batch_size * batch_size
    # x_train = c_images[:buffer_size]
    # y_train = c_labels[:buffer_size]
    # x_train = x_train / 255
    #
    # train_dataset = tf.data.Dataset.from_tensor_slices(x_train).shuffle(buffer_size).batch(batch_size)
    #
    # gan = GAN([img_size, img_size, 3], batch_size, buffer_size)
    # g = tf.keras.models.load_model('models/generator2495') #gan.generator()
    # d = tf.keras.models.load_model("models/discriminator2495") #gan.discriminator()
    # #gan.train(train_dataset, 500, g, d)
    #
    # gan.generate_and_save_images(g, 10)
    print(tf.test.is_built_with_cuda())
